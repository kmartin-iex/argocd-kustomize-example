# ArgoCD Kustomize Example


ArgoCD manifest files for the ["App of Apps" Pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern).
These applications are platform-centric, with capabilities encompassing a range of cross-cutting concerns between platform and application teams.


## Overview

ArgoCD projects and application use this repository as the source-of-truth using GitOps methodology.
Files in this repository should be used only as config and deployment management.
Application code is kept separate from deployment code in this pattern to keep a more clear audit-trail of changes to each environment.
This also serves to de-couple deployed version from application versions.

For more information, see [ArgoCD Best Practices](https://argo-cd.readthedocs.io/en/stable/user-guide/best_practices/#separating-config-vs-source-code-repositories).

## Getting Started

ArgoCD manifest files for applications / application sets (see more [here](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)) are found in the `applications` directory, which uses Kustomize.
The directory hierarchy allows additional applications to be added solely through Git, with normal review by platform / security team, before pushing changes to different environments.

## Tools

ArgoCD supports plain JSON / YAML manifest files, Kustomize, Helm, and other configuration managers through the use of plugins.
See more [here](https://argo-cd.readthedocs.io/en/stable/user-guide/application_sources/).
